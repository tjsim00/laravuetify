import Vue from 'vue';
import VueRouter from 'vue-router';
import basic from './components/BasicComponent';

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: '/basic'
    },
    {
        path: '/basic',
        component: basic,
    },   
]

const router = new VueRouter({routes})

export default router